enablePlugins(JmhPlugin)

name := "fact"

version := "0.1.3"

organization := "actormodel"

scalaVersion := "2.12.0"

sourcesInBase := false

crossPaths := false

//autoScalaLibrary := false

EclipseKeys.projectFlavor := EclipseProjectFlavor.Java

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint", "-encoding", "UTF-8")

unmanagedSourceDirectories in Compile += baseDirectory.value / "src/network/java"

unmanagedSourceDirectories in Compile += baseDirectory.value / "jmh/src/main/java"

libraryDependencies += "junit" % "junit" % "4.12" % "test"

libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"

testOptions in Test := Seq(Tests.Argument(TestFrameworks.JUnit, "-a"))

initialize := {
	val _ = initialize.value
	if (sys.props("java.specification.version") != "1.8")
		sys.error("Java 8 is required for this project.")
}

//artifactName := { 
//	(sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
//		artifact.name + "-" + module.revision + "." + artifact.extension
//}
