package fact.net;

import java.util.UUID;

import fact.Context;
import fact.Reference;

public class Network implements Context {

	public void define() {
		dispatcher("shared", TcpSocket.class, true);
	}
	
	public void open() {
		reply(newSocket());
	}
	
	public void listen(String host, int port) {
		newSocket().send("listen", host, port, sender());
	}
	
	public void listen(String host, int port, Reference driver) {
		newSocket().send("listen", host, port, driver);
	}

	public void connect(String host, int port) {
		newSocket().send("connect", host, port, sender());
	}
	
	public void connect(String host, int port, Reference driver) {
		newSocket().send("connect", host, port, driver);
	}
	
	private final Reference newSocket() {
		return create("socket-" + UUID.randomUUID(), new TcpSocket());
	}
}
