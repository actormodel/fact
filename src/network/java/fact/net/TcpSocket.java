package fact.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.UUID;

import fact.Context;
import fact.Reference;

public final class TcpSocket implements Context {
	public static final long TICK_DELAY = 100;
	
	private SelectableChannel ch;
	
	TcpSocket() { }
	
	TcpSocket(SocketChannel sc) {
		ch = sc;
	}
	
	private SocketChannel sc() {
		return (SocketChannel) ch;
	}
	
	private ServerSocketChannel ssc() {
		return (ServerSocketChannel) ch;
	}
	
	public void define() {
		if(ch != null) 
			become("Register");
	}
	
	//suport request???
	public void connect(String host, int port, Reference driver) {
		try {
			final SocketChannel sc = SocketChannel.open();
			sc.configureBlocking(false);
			sc.connect(new InetSocketAddress(host, port));
			ch = sc;
			setDriver(driver);
			become("Connection");
			send("tick")
				.delay(TICK_DELAY);
		} catch(IOException e) { 
			driver.send("error", e);
		}
	}
	
	//suport request???
	public void listen(String host, int port, Reference driver) {
		try {
			final ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			ssc.bind(new InetSocketAddress(host, port));
			ch = ssc;
			setDriver(driver);
			send("tick").delay(TICK_DELAY);
			driver.send("start");
			become("Listener");
		} catch(IOException e) {
			driver.send("error", e);
		}
	}

	
	public void tick() { }
	
	private final Reference driver() {
		return acq("driver");
	}
	
	private class Closeable {
		
		public void close() {
			try { ch.close(); } 
			catch(IOException e) { } 
			finally { 
				become("Closed");
				driver().send("closed");
			}
		}
		
		protected void close(IOException e) {
			driver().send("error", e);
			close();
		}
	}
	
	private final void setDriver(Reference driver) {
		acq("driver", driver);
	}
	
	public class Register extends Closeable {
		
		public void register(Reference driver) {
			setDriver(driver);
			driver.send("connected");
			become("Client");
			send("tick").delay(TICK_DELAY);
		}
		
	}
	
	public class Client extends Closeable {
		protected ByteBuffer buffer;
		
		public void tick() {
			try {
				read();
				send("tick").delay(TICK_DELAY);
			} catch(IOException e) {
				close(e);
			} 
		}
		
		public void write(ByteBuffer buffer) {
			write(buffer, false);
		}
		
		public void write(ByteBuffer buffer, boolean ack) {
			try {
				final SocketChannel sc = sc();
				while(buffer.hasRemaining())
					sc.write(buffer);
				buffer.clear();
				if(ack)
					driver().send("writed", buffer);
			} catch (IOException e) { close(e); }
		}
		
		protected final void read() throws IOException {
			switch(sc().read(allocate())) {
			case 0: break;
			case -1: close(); break;
			default: readed(); break;
			}
		}
		
		protected final void readed() {
			buffer.flip();
			driver().send("read", buffer);
			buffer = null;
		}
		
		protected final ByteBuffer allocate() {
			if(buffer == null)
				buffer = ByteBuffer.allocate(1024);
			return buffer;
		}
	}
	
	public class Connection extends Client {
		
		public void tick() {
			try {
				read();
				send("tick").delay(TICK_DELAY);
			} catch(NotYetConnectedException e) {
				try {
					if(sc().finishConnect()) {
						driver().send("connected");
						send("tick").delay(TICK_DELAY);
					}
				} catch(IOException e0) { close(e0); }
			} catch(IOException e) { close(e); }
		}
		
	}
	
	public class Listener extends Closeable {
		
		private Reference newSocket(SocketChannel sc) {
			return create("socket-"+ UUID.randomUUID(), new TcpSocket(sc));
		}
		
		public void tick() {
			try {
				final SocketChannel sc = ssc().accept();
				if(sc != null) {
					sc.configureBlocking(false);
					driver().send("accept", newSocket(sc), sc.getRemoteAddress());		
				}
				send("tick").delay(TICK_DELAY);
			} catch(IOException e) { close(e); }
		}
	}
	
	public class Closed {
		
		public void tick() { }
		
		public void close() { }
	
	}
	
}
