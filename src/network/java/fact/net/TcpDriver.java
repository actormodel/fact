package fact.net;

import java.io.IOException;

interface TcpDriver {

	public void error(IOException e);
	
	public void closed();
	
}
