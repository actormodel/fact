package fact.net;

import java.nio.ByteBuffer;

public interface TcpClient extends TcpDriver {

	public void read(ByteBuffer buffer);
	
	public void connected();
}
