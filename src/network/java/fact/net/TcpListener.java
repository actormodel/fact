package fact.net;

import java.net.InetSocketAddress;

import fact.Reference;

public interface TcpListener extends TcpDriver {

	public void start();
	
	public void accept(Reference socket, InetSocketAddress address);
	
}
