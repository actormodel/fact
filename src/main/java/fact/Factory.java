package fact;

import java.util.HashMap;

import fact.Dispatcher.SharedDispatcher;

class Factory {
	private final Rule defaultrule = new Rule(new SingleDispatcherBuilder(), "single", null, false);
	
	final HashMap<Class<?>, Rule> rules = new HashMap<>();
	
	void register(String name, Class<?> type, boolean inheritable) {
		switch(name) {
		case "single": rules.put(type, new Rule(new SingleDispatcherBuilder(), name, type, false)); break;
		case "shared": rules.put(type, new Rule(new SharedDispatcherBuilder(), name, type, inheritable)); break;
		default:
			System.out.println("unknown dispatcher type: " + name);
		}
	}
	
	Dispatcher newDispatcher(Class<?> clazz) {
		return rules.getOrDefault(clazz, defaultrule).builder.newDispatcher();
	}
	
	static class Rule {
		final DispatcherBuilder builder;
		
		final String name;
		
		final Class<?> type;
		
		final boolean inheritable;
		
		Rule(DispatcherBuilder builder, String name, Class<?> type, boolean inheritable) {
			this.builder = builder;
			this.name = name;
			this.type = type;
			this.inheritable = inheritable;
		}
	}
	
	interface DispatcherBuilder {
		Dispatcher newDispatcher();
	}
	
	static class SingleDispatcherBuilder implements DispatcherBuilder {
		public Dispatcher newDispatcher() {
			return new Dispatcher.SingleDispatcher();
		}
	}
	
	static class SharedDispatcherBuilder implements DispatcherBuilder {
		final SharedDispatcher dispatcher = new SharedDispatcher();
		
		@Override
		public Dispatcher newDispatcher() {
			return dispatcher;
		}
	}
}
