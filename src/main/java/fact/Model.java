package fact;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;
import java.util.concurrent.ForkJoinWorkerThread;



public final class Model implements ForkJoinWorkerThreadFactory {
	private static Model instance = null;
	
	public Model() {
		synchronized (Model.class) {
			if(instance != null)
				throw new RuntimeException();
			instance = this;
		}
	}
	
	private Record record = new Record(this, "actor-system", null, new Dispatcher.SingleDispatcher());
	
	public void dead(Envelope envelope) {
		System.out.println("DEAD LETTER " + envelope);
	}
	
	@Override
	public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
		return new Worker(pool);
	}
	
	private void start(String name, Object root) {
		record.create(name, root);
	}
	
	public static void launch(Object root) {
		launch("root", root);
	}
	
	public static void launch(String name, Object root) {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.threadFactory", Model.class.getName());
		((Model) ForkJoinPool.commonPool().getFactory()).start(name, root);
	}
	
	static void deadletter(Envelope envelope) {
		instance.record.in0(new Envelope(envelope.target, instance.record, "dead", new Object[]{envelope}, -1, 0, null, -1));
	}

	//TODO remove
	static void dump() {
		instance.record.dump();
	}

	static Reference find(String address) {
		return instance.record.find(address);
	}
	
}