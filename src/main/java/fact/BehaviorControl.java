package fact;

import java.lang.invoke.MethodHandle;
import java.util.HashMap;
import java.util.LinkedList;

import fact.ActorBuilder.Signature;

class BehaviorControl {
	final Record record;
	
	final Object instance;
	
	final Behavior root;
	
	final HashMap<String, Behavior> behaviors = new HashMap<>();
	
	Behavior current;
	
	final Behavior[] stack = new Behavior[8];
	
	int count = 0;
	
	BehaviorControl(Record record, Object instance, Behavior root) {
		this.record = record;
		this.instance = instance;
		this.root = root;
		current = root;
	}

	void push(String name) {
		if(count == stack.length)
			throw new StackOverflowError("try push behavior");
		stack[count++] = current;
		Behavior behavior = behaviors.get(name);
		if(behavior == null) {
			pop();
			System.out.println("try push behavior with unknown name " + name);
		} else
			current = behavior;
	}
	
	
	void pop() {
		if(count == 0)
			throw new RuntimeException("try next behavior from stack when it empty");
		current = stack[count - 1];
		stack[--count] = null;
	}
	
	final Handle find(String name, Class<?>[] types) {
		Handle handle = current.find(name, types);
		if(handle == null && current != root)
			handle = root.find(name, types);
		return handle;
	}
	
	final void add(String name, Behavior behavior) {
		behaviors.put(name, behavior);
	}
	
	final boolean hasDefineMethod() {
		return root.defining;
	}

	final void become(String name) {
		if(name.equals("define"))
			current = root;
		else
			current = behaviors.getOrDefault(name, root);
	}
	
	static class Behavior {
		private final HandleGroup NULL = new HandleGroup("null");
		
		final Object instance;
		
		final HashMap<String, HandleGroup> table = new HashMap<>();
		
		public Behavior(Object instance) {
			this.instance = instance;
		}

		boolean defining = false;//указывает присутсвует ли define метод
		
		boolean stoping = false;//указывает если ли обработка остановки
		
		boolean resuming = false;
		
		boolean terminating = false;
		
		void register(Handle handle) {
			HandleGroup group = table.get(handle.signature.methodName);
			//TODO move to behavior builder
			if(handle.signature.methodName.equals("define") && handle.signature.ptypes.length == 0)
				defining = true;
			else if(handle.signature.methodName.equals("stop") && handle.signature.ptypes.length == 0)
				stoping = true;
			else if(handle.signature.methodName.equals("resume") && handle.signature.ptypes.length == 0)
				resuming = true;
			else if(handle.signature.methodName.equals("terminate") && handle.signature.ptypes.length == 0)
				terminating = true;
			if(group == null) {
				group = new HandleGroup(handle.signature.methodName);
				table.put(group.name, group);
			}
			group.add(handle);
		}
		Handle find(String name, Class<?>[] types) {
			return table.getOrDefault(name, NULL).find(types);
		}
		
		class HandleGroup {
			final String name;
			
			final LinkedList<Handle> handlers = new LinkedList<>();
			
			HandleGroup(String name) {
				this.name = name;
			}
			
			void add(Handle handle) {
				handlers.add(handle);
			}
			
			Handle find(Class<?>[] types) {
				for(Handle h : handlers)
					if(h.signature.match(types))
						return h;
				return null;
			}
		}
	}
	
	static class Handle {
		final Signature signature;
		
		final MethodHandle handle;
		
		Handle(Signature signature, MethodHandle handle) {
			this.signature = signature;
			this.handle = handle;
		}
		
		Object invoke(Object[] content) throws Throwable {
			return handle.invokeWithArguments(content);
		}
	}

	boolean hasStopHandle() {
		return root.stoping;
	}
	
	boolean hasResumeHandle() {
		return root.resuming;
	}

	public boolean hasTerminateHandle() {
		return root.terminating;
	}

	public String name() {
		if(current == root)
			return "default";
		return current.instance.getClass().getSimpleName();
	}
}
