package fact;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

final class Worker extends ForkJoinWorkerThread {
	private Envelope context;
	
	private Action[] action = new Action[256];
	
	private int count = 0;
	
	Worker(ForkJoinPool pool) {
		super(pool);
		setDaemon(false);
	}

	Record source() {
		return context.source;
	}
	
	Record target() {
		return context.target;
	}
	
	Action out(String name, Object[] content) {
		return out(target(), name, content, 0);
	}
	
	Action out(Record target, String name, Object[] content, long flags) {
		return action(Action.send(target(), target, name, content, flags));
	}

	Action become(String name) {
		return action(Action.become(target(), name));
	}
	
	Reference create(String name, Object instance) {
		return target().create(name, instance);
	}
	
	void setContext(Envelope envelope) {
		context = envelope;
	}

	void flush() {
		for(int i = 0 ; i < count ; i++) {
			action[i].process();
			action[i] = null;
		}
		count = 0;		
	}
	
	//contains buffereds out message
	private final LinkedList<Envelope> buffer = new LinkedList<>();
	
	//buffering out message
	void buffer(Envelope envelope) {
		buffer.add(envelope);
	}
	
	//clear buffer messages
	void clear() {
		buffer.forEach(env -> env.target.in0(env));
		buffer.clear();
	}
	
	Action action(Action a) {
		return action[count++] = a;
	}

	static Worker current() {
		return (Worker) Thread.currentThread();
	}

	public void astop() {
		stop(target());
	}

	public void stop(Record record) {
		out(record, "stop", null, Envelope.STOP);
	}

	public void resume(Record record) {
		out(record, "resume", null, Envelope.RESUME);
	}
	
	void terminate() {
		terminate(target());
	}
	
	void terminate(Record target) {
		out(target, "terminate", null, Envelope.TERMINATE);
	}

	void reply(Object[] content) {
		out(null, "reply", content, Envelope.REPLY);
	}
	
	void complaint(Object[] content) {
		out(null, "reply", content, Envelope.REPLY | Envelope.COMPLAINT);
	}

	public Future request(Record target, String name, Object[] content) {
		return (Future) action(new Future(target(), target, name, content));
	}

	Reference find(String address) {
		Reference reference = 
				address.startsWith("/") 
				? Model.find(address.substring(1))
				: target().find(address);
		return reference != null ? reference : new Reference.NoActor(address);
	}

	boolean isRequest() {
		return context.isRequest();
	}

	boolean isReply() {
		return context.isReply();
	}

	boolean isComplaint() {
		return context.isComplaint();
	}

	void continuation(Record target) {
		out(target, null, null, Envelope.REQUEST | Envelope.CONTINUATION);
	}
}
