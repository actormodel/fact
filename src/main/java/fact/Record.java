package fact;

import java.util.HashMap;
import java.util.LinkedList;

/*
 * Основной класс, для управления акторами
 * - содержит объект-актор
 * - имя актора
 * - полный адреc в системе
 * - указатель на родителя, если нул, то является корнем системы
 * - постоянную ссылку на запись
 * - класс управления поведением обработки сообщений
 * - фабрику, порождение новых дочерних узлов
 * - диспетчер сообщений, обслуживание очереди исполнения
 */
final class Record {
	final Object instance;
	
	final String name;
	
	final Record parent;
	
	final String address;
	
	Dispatcher dispatcher;
	
	final Reference reference = new Reference(this);
	
	final BehaviorControl control;
	
	private Factory factory = new Factory();
	
	Record(Object instance, String name, Record parent, Dispatcher dispatcher) {
		this.instance = instance;
		this.name = name;
		this.parent = parent;
		address = (parent != null ? parent.address + "/" : "") + name;
		this.dispatcher = dispatcher;
		try {
			control = ActorBuilder.from(instance.getClass()).build(this, instance);
		} catch(Throwable e) { e.printStackTrace(); throw new RuntimeException(); }
		if(parent == null)
			behavior = bactive;
	}
	
	private final HashMap<String, Record> childs = new HashMap<>();

	private volatile Envelope target;

	private final LinkedList<Envelope> mailbox = new LinkedList<>();

	private volatile boolean terminate = false;

	private long rtag = -1;
	
	/*
	 * переменная используется при вызове цепочки запросов
	 * сохраняет предыдущее сообщения для дальнейшего использования при ответе
	 */
	Envelope prev;
	
	Factory factory() {
		return factory;
	}
	
	Reference create(String name, Object instance) {
		try {
			Record record = new Record(instance, name, this, factory.newDispatcher(instance.getClass()));
			factory.rules.values().forEach(
					rule -> { 
						if(rule.inheritable) 
							record.factory.rules.put(rule.type, rule);
					}
			);
			synchronized (childs) {
				if(childs.putIfAbsent(name, record) == null) {
					record.in0(new Envelope(parent, this, "define", null, target != null ? target.tag : -1, Envelope.DEFINE, null, -1));
				} else
					System.out.println("actor with address already exists " + record.address);
			}
			return record.reference;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
	
	void become(String name) {
		become(name, false);
	}
	
	void become(String name, boolean request) {
		if(request)
			control.push(name);
		else
			control.become(name);
	}
	
	private synchronized void remove(String name) {
		synchronized (childs) {
			childs.remove(name);
			if(terminate && childs.isEmpty())
				delete();
		}
	}
	
	//self delete
	private void delete() {
		if(!mailbox.isEmpty()) {
			mailbox.forEach(Model::deadletter);
			mailbox.clear();
			acquintances.clear();
		} 
		if(parent != null)
			parent.remove(name);
	}
	
	//call from synchronized free
	private void doTerminate(long atag) {
		synchronized (childs) {
			for(Record item : childs.values())
				item.in0(new Envelope(parent, item, "terminate", null, atag, Envelope.TERMINATE, null, -1));//TODO can invoke when item actor are already terminated, and this message marked as deadletter
		}
	}

	void dump() {
		dump("");
	}
	
	void dump(String prefix) {
		System.out.println(prefix + address);
		synchronized (childs) {
			childs.values().forEach(item -> item.dump("-" + prefix));
		}
	}

	private final HashMap<String, Record> acquintances = new HashMap<>();
	
	Reference acq(String name) {
		if(!acquintances.containsKey(name))
			return new Reference.NoAcq(name);
		return acquintances.get(name).reference;
	}
	
	boolean acq(String name, Reference ref) {
		return acquintances.putIfAbsent(name, ref.record) == null;
	}
	
	@Override
	public String toString() {
		return address;
	}

	public Reference find(String address) {
		String[] split = address.split("/");
		Record target = this;
		for(String name : split)
			if((target = target.get(name)) == null)
				return null;
		return target.reference;
	}
	
	Record get(String name) {
		synchronized (childs) {
			return childs.get(name);
		}
	}
	
	private final  long rtag() {
		return prev != null ? prev.rtag : target.rtag;
	}

	private final Record to() {
		return prev != null ? prev.to : target.to;
	}
	
	private final Task task = new Task(this);
	
	//=========================================
	//Control methods||||||||||||||||||||||||||
	//=========================================
	
	synchronized void in0(Envelope envelope) { 
		behavior.in(envelope);
	}
	
	Envelope out0(Record record, String name, Object[] content, long outflags) {
		return behavior.out(record, name, content, outflags);
	}
	
	void process0() { 
		//reset before use
		flags = 0;
		result = null;
		complaint = false;
		Worker.current().setContext(target);//set context
		behavior.process();//process message
		Worker.current().flush();//flush actions
		dispatcher.release(task);
		behavior.free();//free record after processing
		Worker.current().clear();//clear envelope output buffer
		Worker.current().setContext(null);//clear context
		//clean because can link on not useable object, and next processing
		result = null;//may create dead link on not usable object
	}
	
	synchronized void free0() { 
		behavior.free();
	}
	
	//=========================================
	//Record behavior||||||||||||||||||||||||||
	//=========================================
	private final void dispatch() {
		dispatcher.dispatch(task);
	}
	
	private final void next() {
		if(!mailbox.isEmpty())
			next(mailbox.removeFirst());
	}
	
	private final void next(Envelope envelope) {
		target = envelope;
		if(target.isSystem()) {
			if(target.isStop()) 
				behavior = bstop;
			if(target.isTerminate())
				behavior = bterminate;
		} else if(target.isRequest())
			behavior = bprequest;//установить обработчик запроса
		dispatch();
	}
	
	//Содредит результат обработки метода, модет использоватся как ответ по умолчанию
	private Object result = null;
	
	//сигнализирует успешно ли обработан методи, или возникла ошибка
	private boolean complaint = false;

	//флаги исходящих сообщений
	//Фиксирует, за время обработки, был ли ответ на запрос, был ли запрос и т.д
	private long flags = 0;

	//Для наглядного кода, вместа Record.this
	private final Record self = this;
	
	private final Active bactive = new Active();
	private final Define bdefine = new Define();
	private final ProcessingRequest bprequest = new ProcessingRequest();
	private final Request brequest = new Request();
	private final Stop bstop = new Stop();
	private final Terminate bterminate = new Terminate();
	private Behavior behavior = bdefine;
	
	private interface Behavior {
		
		void in(Envelope envelope);
		
		Envelope out(Record record, String name, Object[] content, long outflags);
		
		void process();
		
		void free();
	}
	
	private class Active implements Behavior {

		@Override
		public void in(Envelope envelope) {
			if(target == null) {
				next(envelope);
			} else
				mailbox.add(envelope);
		}

		@Override
		public Envelope out(Record record, String name, Object[] content, final long outflags) {
			if(Envelope.isSystem(outflags))
				return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
			
			if(Envelope.isRequest(outflags)) {
				if(Envelope.isRequest(flags))
					throw new RuntimeException("second time request");
				if(record == self)
					throw new RuntimeException("recursion request");
				flags = outflags;
				rtag = target.tag;
				return new Envelope(self, record, name, content, target.tag, outflags, self, target.tag);
			}
			if(Envelope.isReply(outflags))
				throw new RuntimeException("try reply on non request message " + target);
			if(Envelope.isContinuation(outflags))
				throw new RuntimeException("try continuation on non request message");
			return new Envelope(self, record, name, content, target.tag, outflags, null, -1);	
		}

		@Override
		public void process() {
			try {
				result = control.find(target.name, target.types()).invoke(target.content);
			} catch (Throwable e) { 
				result = e; 
				complaint = true;
			}
		}

		@Override
		public void free() {
			if(Envelope.isRequest(flags)) {
				rtag = target.tag;
				behavior = brequest;//установить для ожидания ответа
				target = null;
				return;
			}
			target = null;
			next();
		}
	}
	
	
	private class Define extends Active {
		
		@Override
		public void in(Envelope envelope) {
			if(envelope.isDefine()) {
				if(target != null) 
					throw new RuntimeException("not null target on define message");
				target = envelope;
				dispatch();
			} else
				mailbox.add(envelope);
		}

		/*
		 * throws RuntimeException if target is not define envelope
		 */
		@Override
		public void process() {
			if(target.isDefine()) {
				if(control.hasDefineMethod())
					try {
						result = control.find(target.name, target.types()).invoke(target.content);
					} catch (Throwable e) { 
						result = e; 
						complaint = true;
					}
			} else
				throw new RuntimeException("not define envelope");
		}
			
		@Override
		public void free() {
			behavior = bactive;
			super.free();
		}
	}
	
	/*
	 * Данное поведения устанавливается при обработке входящего сообщения как запроса
	 * Active.in
	 */
	private class ProcessingRequest extends Active {
		
		@Override
		public void in(Envelope envelope) {
			mailbox.add(envelope);
		}
		
		@Override
		public Envelope out(Record record, String name, Object[] content, long outflags) {
			if(Envelope.isSystem(outflags))
				return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
			if(Envelope.isContinuation(outflags)) {
				if(Envelope.isContinuation(flags))
					throw new RuntimeException("more than one continuation per request");
				if(Envelope.isReply(flags))
					throw new RuntimeException("cant coninue when reply");
				flags = outflags;
				return new Envelope(self, record, target.name, target.content, target.tag, outflags, to(), rtag());
			}
			
			if(Envelope.isRequest(outflags)) {
				if(Envelope.isRequest(flags))
					throw new RuntimeException("second time request");
				if(record == self)
					throw new RuntimeException("recursion request");
				rtag = target.tag;
				flags = outflags;
				return new Envelope(self, record, name, content, target.tag, outflags,self, rtag);
			} 
			
			if(Envelope.isReply(outflags)) {
				if(Envelope.isReply(flags))
					throw new RuntimeException("more than one reply per request");
				if(Envelope.isContinuation(flags))
					throw new RuntimeException("cant reply when continue");
				flags = outflags;
				return new Envelope(self, to(), name, content, target.tag, outflags, null, rtag());
			}
			return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
		}
		
		@Override
		public void free() {
			behavior = bactive;
			if(Envelope.isRequest(flags)) {
				prev = target;
				target = null;
				behavior = brequest;
				return;
			}
			if(!Envelope.isReply(flags) && !Envelope.isContinuation(flags)) {
				System.out.println("no reply");
				to().in0(new Envelope(self, to(), "reply", new Object[]{result}, target.tag, Envelope.REPLY | (complaint ? Envelope.COMPLAINT : 0), null, rtag()));	
			}
			target = null;
			next();
		}
	}
	
	/*
	 * Обработка ответа на запрос
	 */
	private class Request extends ProcessingRequest {
		
		@Override
		public void in(Envelope envelope) {
			if(envelope.isReply()) {
				if(envelope.rtag == rtag) {
					target = envelope;
					dispatch();
				} else
					throw new RuntimeException("receive reply with wrong tag");
			} else
				mailbox.add(envelope);
		}
		
		@Override
		public Envelope out(Record record, String name, Object[] content, long outflags) {
			if(prev == null) {
				if(Envelope.isSystem(outflags))
					return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
				if(Envelope.isRequest(outflags)) {
					if(Envelope.isRequest(flags))
						throw new RuntimeException("second time request");
					if(record == self)
						throw new RuntimeException("recursion request");
					flags = outflags;
					rtag = target.tag;
					return new Envelope(self, record, name, content, target.tag, outflags, self, target.tag);
				}
				if(Envelope.isReply(outflags))
					throw new RuntimeException("try reply on non request message " + target);
				if(Envelope.isContinuation(outflags))
					throw new RuntimeException("try continuation on non request message");
				return new Envelope(self, record, name, content, target.tag, outflags, null, -1);	
			} else 
				return super.out(record, name, content, outflags);
		}
		
		@Override
		public void process() {
			try {
				super.process();
			} finally { control.pop(); }
		}
		
		@Override
		public void free() {
			behavior = bactive;
			if(prev == null) {
				target = null;
				if(Envelope.isRequest(flags)) {
					behavior = brequest;
					return;
				}
				next();
			} else {
				if(Envelope.isRequest(flags))
					return;
				if(!Envelope.isReply(flags) && !Envelope.isContinuation(flags)) 
					to().in0(new Envelope(self, to(), "reply", new Object[]{result}, target.tag, Envelope.REPLY | (complaint ? Envelope.COMPLAINT : 0), null, rtag()));
				prev = null;
				target = null;
			}
		}
	}
	
	private class Stop extends Active {

		@Override
		public void in(Envelope envelope) {
			if(target == null) {
				if(envelope.isResume()) {
					target = envelope;
					dispatch();
				} else
					mailbox.add(envelope);
			} else
				mailbox.add(envelope);
		}

		@Override
		public Envelope out(Record record, String name, Object[] content, long outflags) {
			if(Envelope.isSystem(outflags))
				return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
			if(target.isStop()) {
				if(Envelope.isRequest(outflags))
					throw new RuntimeException("request while stoping");
				return super.out(record, name, content, outflags);
			} else
				return super.out(record, name, content, outflags);
		}

		@Override
		public void process() {
			if(target.isStop() && control.hasStopHandle()) {
				try {
					control.find(target.name, target.types()).invoke(target.content);
				} catch (Throwable e) { e.printStackTrace(); System.out.println("stoping problem"); }
			} else if(target.isResume() && control.hasResumeHandle())
				try {
					control.find(target.name, target.types()).invoke(target.content);
				} catch (Throwable e) { e.printStackTrace(); System.out.println("resume problem"); }
		}

		@Override
		public void free() {
			if(target.isResume())
				behavior = bactive;
			target = null;
			next();
		}
	}
	
	private class Terminate extends Stop {
		
		@Override
		public void in(Envelope envelope) {
			Model.deadletter(envelope);
		}

		@Override
		public Envelope out(Record record, String name, Object[] content, long outflags) {
			if(record == self)
				throw new RuntimeException("recursive message when terminating");
			if(Envelope.isSystem(outflags))
				return new Envelope(self, record, name, content, target.tag, outflags, null, -1);
			if(Envelope.isRequest(outflags))
				throw new RuntimeException("request while terminating");
			return super.out(record, name, content, outflags);
		}
		
		@Override
		public void process() {
			try {
				if(control.hasTerminateHandle())
					control.find(target.name, target.types()).invoke(target.content);
			} catch (Throwable e) { e.printStackTrace(); System.out.println("terminate problem " + target); }
		}
		
		@Override
		public void free() {
			synchronized (childs) {
				terminate = true;
				if(childs.isEmpty())
					delete();
				else
					doTerminate(target.tag);
			}
			target = null;
		}
	}
}
