package fact;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import fact.BehaviorControl.Behavior;
import fact.BehaviorControl.Handle;

class ActorBuilder {
	private static final Lookup lookup = MethodHandles.lookup();
	
	private static final HashMap<Class<?>, ActorBuilder> builders = new HashMap<>();
	
	final Class<?> clazz;
	
	final BehaviorBuilder root;//root behavior builder
	
	final ArrayList<BehaviorBuilder> behaviors = new ArrayList<BehaviorBuilder>();
	
	ActorBuilder(Class<?> clazz) throws Throwable {
		root = new BehaviorBuilder(clazz);
		this.clazz = clazz;
		for(Class<?> c : clazz.getClasses()) {
			if(!Modifier.isStatic(c.getModifiers()) && Modifier.isPublic(c.getModifiers()) && !c.isInterface())
				behaviors.add(new BehaviorBuilder(clazz, c));
		}
	}
	
	public static synchronized ActorBuilder from(Class<?> clazz) throws Throwable {
		if(!builders.containsKey(clazz)) {
			final ActorBuilder builder = new ActorBuilder(clazz);
			builders.put(clazz, builder);
			return builder;
		}
		return builders.get(clazz);
	}
	
	BehaviorControl build(Record record, Object instance) throws Throwable {
		if(instance.getClass() != clazz)
			throw new RuntimeException();
		final BehaviorControl control = new BehaviorControl(record, instance, root.build(instance));
		for(BehaviorBuilder builder : behaviors)
			control.add(builder.subclass.getSimpleName(), builder.build(instance));
		return control;
	}
	
	static class Signature {
		final String methodName;
		
		final Class<?>[] ptypes;
		
		final Class<?> rtype;
		
		Signature(String methodName, Class<?>[] ptypes, Class<?> rtype) {
			this.methodName = methodName;
			this.ptypes = ptypes;
			this.rtype = rtype;
		}

		boolean match(Class<?>[] signature) {
			if(ptypes.length != signature.length)
				return false;
			for(int i = 0 ;  i < signature.length ; i++)
				if(!check(ptypes[i], signature[i]))
					return false;
			return true;
		}
	}
	
	private final static boolean check(Class<?> left, Class<?> right) {
		if(!left.isArray() && left.isPrimitive()) {
			if(right == Byte.class && left == byte.class) return true;
			else if(right == Short.class && left == short.class) return true;
			else if(right == Integer.class && left == int.class) return true;
			else if(right == Long.class && left == long.class) return true;
			else if(right == Float.class && left == float.class) return true;
			else if(right == Double.class && left == double.class) return true;
			else if(right == Boolean.class && left == boolean.class) return true;
			else if(right == Character.class && left == char.class) return true;
			else return false;
		} else return left.isAssignableFrom(right);
	}
	
	static class BehaviorBuilder {
		final Class<?> clazz;
		
		final Class<?> subclass;
		
		final boolean root;
			
		final ArrayList<Signature> methods = new ArrayList<>();
		
		final Constructor<?> constructor;
		
		BehaviorBuilder(Class<?> clazz) throws Throwable {
			this(clazz, null);
		}
		
		BehaviorBuilder(Class<?> clazz, Class<?> subclass) throws Throwable {
			this.clazz = clazz;
			this.root = subclass == null ? true : false;
			this.subclass = subclass;	
			if(root) {
				constructor = null;
				parse(clazz);
			} else {
				constructor = subclass.getConstructor(clazz);
				parse(subclass);
			}
		}
		
		private void parse(Class<?> c) { 
			for(Method m : c.getMethods()) {
				if(m.getDeclaringClass() != Object.class && !m.getDeclaringClass().isInterface() && Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers()))
					methods.add(new Signature(m.getName(), m.getParameterTypes(), m.getReturnType()));
			}
		}

		Behavior build(Object instance) throws Throwable {
			Object i = instance;
			Behavior behavior;
			
			if(root) {
				behavior = new Behavior(instance);
			} else {
				i = constructor.newInstance(instance);
				behavior = new Behavior(i);
			}
			for(Signature s : methods)
				behavior.register(new Handle(s, lookup.bind(i, s.methodName, MethodType.methodType(s.rtype, s.ptypes))));
			return behavior;
		}

		public void dump() {
			if(root)
				System.out.println("# behavior " + clazz.getName());
			else
				System.out.println("# behavior " + subclass.getName());
			for(Signature signature : methods) {
				System.out.println("## " + signature.methodName + " " + Arrays.toString(signature.ptypes));
			}
		}
	}

	public void dump() {
		System.out.println("# root" + clazz.getName());
		root.dump();
		System.out.println("# subclasses");
		behaviors.forEach(item -> item.dump());
	}
	
}
