package fact;

public abstract class Action {
	long delay = 0;
	
	Action() { }
	
	abstract boolean isDelayed();
	
	abstract void process();

	public void delay(long time) {
		delay = time;
	}
	
	static class SendAction extends Action {
		Record source;
		
		Record target;
		
		String name;
		
		Object[] content;
		
		long flags;
		
		SendAction(Record source, Record target, String name, Object[] content, long flags) {
			this.source = source;
			this.target = target;
			this.name = name;
			this.content = content;
			this.flags = flags;
		}
			
		@Override
		void process() {
			final Envelope envelope = source.out0(target, name, content, flags);
			if(delay > 0) {
				DelayedService.service.add(envelope, System.currentTimeMillis() + delay);
			} else
				Worker.current().buffer(envelope);
			source = target = null;
			name = null;
			content = null;
		}

		@Override
		boolean isDelayed() {
			return true;
		}
	
	}
	
	static class BecomeAction extends Action {
		Record target;
		
		final String name;
		
		BecomeAction(Record target, String name) {
			this.target = target;
			this.name = name;
		}
		
		@Override
		void process() {
			target.become(name);
			target = null;
		}

		@Override
		boolean isDelayed() {
			return false;
		}
	}
	
	static Action become(Record target, String name) {
		return new BecomeAction(target, name);
	}
	
	static Action send(Record source, Record target, String name, Object[] content, long flags) {
		return new SendAction(source, target, name, content, flags);
	}
}
