package fact;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

final class Envelope {
	private final AtomicLong generator = new AtomicLong();
	
	private final static Object[] EMPTY = new Object[0];
	
	final Record source;
	
	final Record target;
	
	final String name;
	
	final Object[] content;
	
	Class<?>[] types = null;
	
	final long tag = generator.getAndIncrement() & Long.MAX_VALUE;
	
	final long atag;//номер сообщения в процессе обработки готорого было сгененрированно данное сообщение
	
	final long flags;
	
	final Record to;
	
	final long rtag;
	
	Envelope(Record source, Record target, String name, Object[] content, long atag, long flags, Record to, long rtag) {
		this.source = source;
		this.target = target;
		this.name = name;
		this.content = content == null ? EMPTY : content;
		this.atag = atag;
		this.flags = flags;
		this.to = to;
		this.rtag = rtag;
	}
	
	final Class<?>[] types() {
		if(types == null) {
			types = new Class<?>[content.length];
			for(int i = 0 ; i < types.length ; i++)
				types[i] = content[i].getClass();
		}
		return types;
	}

	final boolean isStop() {
		return flags == STOP;
	}
	
	final boolean isTerminate() {
		return flags == TERMINATE;
	}

	final boolean isRequest() {
		return (flags & REQUEST) != 0;
	}
	
	final boolean isReply() {
		return (flags & REPLY) != 0;
	}
	
	final boolean isComplaint() {
		return (flags & COMPLAINT) != 0;
	}
	
	final boolean isFinite() {
		return (flags & FINITE) != 0;
	}
	
	final boolean isContinuation() {
		return (flags & CONTINUATION) != 0;
	}
	
	final boolean isSystem() {
		return (flags & FLAG_SYSTEM) != 0;
	}
	
	static final long REQUEST = 0x01;//id
	static final long REPLY = 0x02;//id
	static final long COMPLAINT = 0x04;//flags
	static final long CONTINUATION = 0x20;//flags
	static final long FINITE = 0x40;//flags
	
	static final long FLAG_SYSTEM = Long.MIN_VALUE;
	
	//system messages
	static final long DEFINE = FLAG_SYSTEM | 0x1;//id
	static final long STOP = FLAG_SYSTEM | 0x2;//id
	static final long RESUME = FLAG_SYSTEM | 0x3;//id
	static final long TERMINATE = FLAG_SYSTEM | 0x4;//id
	static final long REGISTER = FLAG_SYSTEM | 0x5;//id
	
	@Override
	public String toString() {
		return "E " + target + " <~~ " + name + " " + Arrays.toString(types());
	}

	final boolean isDefine() { 
		return flags == DEFINE;
	}
	
	final boolean isResume() {
		return flags == RESUME;
	}
	
	static boolean isRequest(long flags) {
		return (REQUEST & flags) != 0;
	}
	
	static boolean isReply(long flags) {
		return (REPLY & flags) != 0;
	}
	
	static boolean isComplaint(long flags) {
		return (COMPLAINT & flags) != 0;
	}
	
	static boolean isContinuation(long flags) {
		return (CONTINUATION & flags) != 0;
	}

	public static boolean isSystem(long flags) {
		return (flags & FLAG_SYSTEM) != 0;
	}
}