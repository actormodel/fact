package fact;

public class Reference {
	Record record;
	
	Reference(Record record) {
		this.record = record;
	}
	
	public Action send(String name, Object...content) {
		return Worker.current().out(record, name, content, 0);
	}
	
	public Future request(String name, Object...content) {
		return Worker.current().request(record, name, content);
	}
	
	public void stop() {
		Worker.current().stop(record);
	}

	public void resume() {
		Worker.current().resume(record);
	}
	
	public void kill() {
		Worker.current().terminate(record);
	}

	public boolean isNull() {
		return false;
	}
	
	@Override
	public String toString() {
		return "" + record;
	}
	
	
	static class NoAcq extends Reference {
		String name;
		
		NoAcq(String name) {
			super(null);
			this.name = name;
		}
		
		@Override
		public Action send(String name, Object...content) {
			System.out.println("try send to unknown acquintance " + name);
			return null;
		}
		
		@Override
		public Future request(String name, Object...content) {
			System.out.println("try request to unknown acquintance " + name);
			return null;
		}
		
		@Override
		public void stop() {
			System.out.println("try stop unknown acquintance " + name);
		}

		@Override
		public void resume() {
			System.out.println("try resume unknown acquintance " + name);
		}
		
		@Override
		public void kill() {
			System.out.println("try kill unknown acquintance " + name);
		}
		
		@Override
		public boolean isNull() {
			return true;
		}
	}
	
	static class NoActor extends Reference {
		String address;
		
		NoActor(String address) {
			super(null);
			this.address = address;
		}
		
		@Override
		public Action send(String name, Object...content) {
			System.out.println("try send to unknown actor " + address);
			return null;
		}
		
		@Override
		public Future request(String name, Object...content) {
			System.out.println("try request to unknown actor " + address);
			return null;
		}
		
		@Override
		public void stop() {
			System.out.println("try stop unknown actor " + address);
		}

		@Override
		public void resume() {
			System.out.println("try resume unknown actor " + address);
		}
		
		@Override
		public void kill() {
			System.out.println("try kill unknown actor " + address);
		}
		
		public boolean isNull() {
			return true;
		}
	}
}
