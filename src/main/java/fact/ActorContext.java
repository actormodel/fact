package fact;

public final class ActorContext {

	private ActorContext() { }
	
	public static Action send(String name, Object...content) {
		return Worker.current().out(name, content);
	}
	
	public static void reply(Object...content) {
		Worker.current().reply(content);
	}
	
	public static void complaint(Object...content) {
		Worker.current().complaint(content);
	}
	
	public static void continuation(Reference target) {
		Worker.current().continuation(target.record);
	}
	
	public static void become(String name) {
		Worker.current().become(name);
	}
	
	public static Reference sender() {
		return Worker.current().source().reference;
	}
	
	public static Reference target() {
		return Worker.current().target().reference;
	}
	
	public static Reference acq(String name) {
		return Worker.current().target().acq(name);
	}
	
	public static boolean acq(String name, Reference reference) {
		return Worker.current().target().acq(name, reference);
	}
	
	public static Reference actor(String address) {
		return Worker.current().find(address);
	}
	
	public static Reference create(String name, Object instance) {
		return Worker.current().create(name, instance);
	}
	
	public static void dispatcher(String type, Class<?> clazz) {
		dispatcher(type, clazz, false);
	}
	
	public static void dispatcher(String type, Class<?> clazz, boolean inheritable) {
		Worker.current().target().factory().register(type, clazz, inheritable);
	}
	
	public static boolean isReply() {
		return Worker.current().isReply();
	}
	
	public static boolean isComplaint() {
		return Worker.current().isComplaint();
	}
	
	public static boolean isRequest() {
		return Worker.current().isRequest();
	}
	
	public static void stop() {
		Worker.current().astop();
	}
	
	public static void kill() {
		Worker.current().terminate();
	}
	
}

