package fact;

import java.util.Iterator;
import java.util.LinkedList;

final class DelayedService extends Thread {
	final static DelayedService service = new DelayedService();

	private final LinkedList<Entry> queue = new LinkedList<>();
	
	private DelayedService() {
		start();
	}
	
	synchronized void add(Envelope envelope, long time) {
		queue.add(new Entry(envelope, time));
	}
	
	@Override
	public void run() {
		while(true) {
			long time = System.currentTimeMillis();
			synchronized (this) {
				Iterator<Entry> it = queue.iterator();
				while(it.hasNext()) {
					Entry entry = it.next();
					if(entry.time <= time) {
						it.remove();
						entry.flush();
					}
				}
			}
			Thread.yield();
		}
	}
	
	final class Entry {
		final Envelope envelope;
		
		final long time;
		
		Entry(Envelope envelope, long time) {
			this.envelope = envelope;
			this.time = time;
		}
		
		void flush() {
			envelope.target.in0(envelope);
		}
	}
}
