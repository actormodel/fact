package fact;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;

public abstract class Dispatcher {

	public abstract void dispatch(Task envelope);
	
	public abstract void release(Task envelope);
	
	static class SingleDispatcher extends Dispatcher {

		@Override
		public void dispatch(Task task) {
			ForkJoinPool.commonPool().execute(task);
		}

		@Override
		public void release(Task task) { }
		
	}
	
	static final class SharedDispatcher extends Dispatcher {
		private LinkedList<Task> queue = new LinkedList<>();
		
		private Task target;
		
		@Override
		public synchronized void dispatch(Task task) {
			queue.add(task);
			if(target == null)
				ForkJoinPool.commonPool().execute(target = task);
		}

		@Override
		public synchronized void release(Task envelope) {
			if(target == envelope) {
				target = null;
				if(!queue.isEmpty())
					ForkJoinPool.commonPool().execute(target = queue.removeFirst());
			} else
				System.out.println("shared dispatcher error");
		}
	}
}
