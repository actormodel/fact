package fact;

import fact.Action.SendAction;

public final class Future extends SendAction {
	private String handle;
	
	Future(Record source, Record target, String name, Object[] content) {
		super(source, target, name, content, Envelope.REQUEST);
	}

	public void handle(String handle) {
		this.handle = handle;
	}
	
	@Override
	void process() {
		if(handle == null)
			throw new RuntimeException("dont setup handler for request");
		delay = 0;
		source.become(handle, true);
		super.process();
	}
}
