package fact;

public interface Context {

	default Action send(String name, Object...content) {
		return Worker.current().out(name, content);
	}
	
	default Action become(String name) {
		return Worker.current().become(name);
	}
	
	default Reference create(String name, Object instance) {
		return Worker.current().create(name, instance);
	}
	
	default void kill() {
		Worker.current().terminate();
	}
	
	default Reference sender() {
		return Worker.current().source().reference;
	}
	
	default Reference self() {
		return Worker.current().target().reference;
	}
	
	default void reply(Object...content) {
		Worker.current().reply(content);
	}
	
	default void complaint(Object...content) {
		Worker.current().complaint(content);
	}
	
	default void continuation(Reference target) {
		Worker.current().continuation(target.record);
	}
	
	default void dispatcher(String type, Class<?> clazz) {
		Worker.current().target().factory().register(type, clazz, false);
	}
	
	default void dispatcher(String type, Class<?> clazz, boolean inheritable) {
		Worker.current().target().factory().register(type, clazz, inheritable);
	}
	
	default Reference acq(String name) {
		return Worker.current().target().acq(name);
	}
	
	default boolean acq(String name, Reference reference) {
		return Worker.current().target().acq(name, reference);
	}
	
	default Reference actor(String address) {
		return Worker.current().find(address);
	}
	
	default boolean isRequest() {
		return Worker.current().isRequest();
	}
	
	default boolean isReply() {
		return Worker.current().isReply();
	}
	
	default boolean isComplaint() {
		return Worker.current().isComplaint();
	}
	
	default void dump() {
		Model.dump();
	}
}
