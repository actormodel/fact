package fact;

import java.util.concurrent.ForkJoinTask;

class Task extends ForkJoinTask<Void> {
	final Record record;
	
	Task(Record record) {
		this.record = record;
	}

	@Override
	public Void getRawResult() { return null; }

	@Override
	protected void setRawResult(Void value) { }

	@Override
	protected boolean exec() {
		try {
			record.process0();
		} catch (Throwable e) { e.printStackTrace(); System.out.println(record); }
		return false;
	}

	private static final long serialVersionUID = -8448911089718563695L;
}
