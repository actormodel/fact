package fact.samples;

import fact.Model;

public class DefineSample {

	public void define() {
		System.out.println("hello define!");
	}
	
	public static void main(String...args) {
		Model.launch(new DefineSample());
	}
}
