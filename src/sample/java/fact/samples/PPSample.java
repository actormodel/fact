package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;

//ping pong sample
public class PPSample implements Context {

	public void define() {
		create("ping", new PingActor()).send("start", create("pong", new PongActor()));
	}
	
	public static void main(String...args) {
		Model.launch(new PPSample());
	}
	
	public static class PongActor implements Context {

		public void message(String m) {
			System.out.println("pong receive " + m);
			sender().send("message", "pong");
		}
	}
	
	public static class PingActor implements Context {

		public void start(Reference ref) {
			ref.send("message", "ping");
		}
		
		public void message(String message) {
			System.out.println("ping receive " + message);
			sender().send("message", "ping");
		}
	}
}
