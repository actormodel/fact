package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;

public class ConsoleSample implements Context {

	public void define() {
		Reference ref = create("console", System.out);
		for(char c : "hello world!".toCharArray())
			ref.send("println", c);
		for(int i = 0 ; i < Byte.MAX_VALUE ; i++)
			ref.send("println", "i=" + i);
		ref.send("println", "hello world!");
		ref.send("println", ref);
	}
	
	public static void main(String...args) {
		Model.launch(new ConsoleSample());
	}
}
