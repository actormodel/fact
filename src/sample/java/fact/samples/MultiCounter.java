package fact.samples;

import fact.Model;
import static fact.ActorContext.*;

public class MultiCounter {

	public void define() {
		for(int i = 0 ; i < 1000 ; i++)
			create("i-"+i, new Counter());
	}
	
	public static void main(String...args) {
		Model.launch(new MultiCounter());
	}
}
