package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;

public class PingPong implements Context {
	private Type type;
	
	public PingPong(Type type) {
		this.type = type;
	}
	
	public void define() {
		switch(type) {
		case Ping: become("Ping"); break;
		case Pong: become("Pong"); break;
		}
	}
	
	public class Pong {
		
		public void start(Reference target) {
			target.send("message", "pong");
		}
		
		public void message(String m) {
			System.out.println("pong receive " + m);
			sender().send("message", "pong");
		}
		
	}
	
	public class Ping {
		
		public void start(Reference target) {
			target.send("message", "ping");
		}
		
		public void message(String m) {
			System.out.println("ping receive " + m);
			sender().send("message", "ping");
		}
		
	}
	
	public static void main(String...args) {
		Model.launch(new Start());
	}
	
	public static class Start implements Context {
		
		public void define() {
			create("pong",  new PingPong(Type.Pong)).send("start", create("ping", new PingPong(Type.Ping)));
		}
	}
	
	enum Type { Ping, Pong }
}
