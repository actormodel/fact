package fact.samples;

import static fact.ActorContext.*;

import fact.ActorContext;
import fact.Model;

public class RequestChainSample {

	public static class A1 {
	
		public void define() {
			System.out.println("RQ 1");
			create("A2", new A2()).request("rq").handle("Request");
			send("print");
		}
		
		public void print() {
			System.out.println("print");
		}
		
		public class Request {
			
			public void reply() {
				System.out.println("RP 1");
			}
			
		}
	}
	
	public static class A2 {
		
		public void define() {
			acq("A3", create("A3", new A3()));
		}
		
		public void rq() {
			System.out.println("RQ 2");
			acq("A3").request("rq").handle("Request");
		}
		
		public class Request {
			
			public void reply() {
				System.out.println("RP 2");
				ActorContext.reply();
			}
		}
	}
	
	public static class A3 {
		public void define() {
			acq("A4", create("A4", new A4()));
		}
		
		public void rq() {
			System.out.println("RQ 3");
			acq("A4").request("rq").handle("Request");
		}
		
		public class Request {
			
			public void reply() {
				System.out.println("RP 3");
				ActorContext.reply();
			}
		}
	}
	
	public static class A4 {
		public void rq() {
			System.out.println("RQ 4");
			ActorContext.reply();
		}
	}
	
	public static void main(String...args) {
		Model.launch(new A1());
	}
}
