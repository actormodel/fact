package fact.samples;

import fact.Context;
import fact.Model;

public class CounterDelay implements Context {
	private long count = 0;
	
	public void define() {
		send("print").delay(1000);
		send("tick");
	}
	
	public void tick() {
		count++;
		send("tick");
	}
	
	public void print() {
		System.out.println(count);
		count = 0;
		send("print").delay(1000);
	}
	
	public static void main(String...args) {
		Model.launch(new CounterDelay());
	}
}
