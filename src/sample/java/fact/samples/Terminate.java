package fact.samples;

import fact.Context;
import fact.Model;

public class Terminate implements Context {

	public void define() {
		send("test").delay(1000);
	}
	
	public void test() {
		System.out.println("start terminating");
		kill();
	}
	
	public void terminate() {
		System.out.println("terminating");
	}
	
	public static void main(String...args) {
		Model.launch(new Terminate());
	}
}
