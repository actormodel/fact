package fact.samples;

import fact.Context;
import fact.Model;

public class BehaviorTest implements Context {

	public void define() {
		become("X");
		send("x");
		send("y");
	}
	
	public void x() {
		System.out.println("default x");
	}
	
	public void y() {
		System.out.println("default y");
		become("Y");
		send("x");
		send("y");
	}
	
	public class X {
		
		public void x() {
			System.out.println("x");
		}
		
	}
	
	public class Y extends X {
		
		public void y() {
			System.out.println("y");
		}
		
	}
	
	public static void main(String...args) {
		Model.launch(new BehaviorTest());
	}
}
