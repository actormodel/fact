package fact.samples;

import fact.Context;
import fact.Model;

public class DelaySample1 implements Context {
	
	public void define() {
		for(int i = 0 ; i < 1000 ; i++)
			create("delay-" + i, new DelaySample0());
	}
	
	public static void main(String...args) {
		Model.launch(new DelaySample1());
	}
}
