package fact.samples;

import java.util.Objects;

import fact.Context;
import fact.Model;
import fact.Reference;

public class PPSample0 implements Context {

	public void define() {
		create("ping", new TestActor(Type.Ping))
		.send("start", 
				create("pong", new TestActor(Type.Pong))
		);		
	}
	
	public static class TestActor implements Context {
		final Type type;
		
		TestActor(Type type) {
			Objects.requireNonNull(type);
			this.type = type;
		}
		
		public void define() {
			switch(type) {
			case Ping: become("Ping"); break;
			case Pong: become("Pong"); break;
			}
		}
		
		public void start(Reference target) {
			target.send("message", "pong");
		}
		
		public class Ping {
			public void message(String m) {
				System.out.println("ping receive " + m);
				sender().send("message", "ping");
			}
		}
		
		public class Pong {
			public void message(String m) {
				System.out.println("pong receive " + m);
				sender().send("message", "pong");
			}
		}
	}
	
	enum Type { Ping, Pong }
	
	public static void main(String...args) {
		Model.launch(new PPSample0());
	}
}
