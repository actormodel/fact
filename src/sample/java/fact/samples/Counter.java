package fact.samples;

import fact.Context;
import fact.Model;

public class Counter implements Context {
	long count = 0;
	
	long time = System.currentTimeMillis() + 1000;
	
	public void define() {
		send("tick");
	}
	
	public void tick() {
		count++;
		if(time <= System.currentTimeMillis()) {
			System.out.println(count);
			count = 0;
			time = System.currentTimeMillis() + 1000;
		}
		send("tick");
	}
	
	public static void main(String...args) {
		Model.launch(new Counter());
	}
	
	
}
