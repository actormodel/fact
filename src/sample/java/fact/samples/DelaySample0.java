package fact.samples;

import fact.Context;
import fact.Model;

public class DelaySample0 implements Context{
	private long time = System.currentTimeMillis();
	
	public void define() {
		send("print").delay(100);
	}
	
	public void print() {
		System.out.println("print " + (System.currentTimeMillis() - time));
		time = System.currentTimeMillis();
		send("print").delay(100);
	}
	
	public static void main(String...args) {
		Model.launch(new DelaySample0());
	}
}
