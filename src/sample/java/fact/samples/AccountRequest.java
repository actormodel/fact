package fact.samples;

import fact.Context;

public class AccountRequest implements Context {
	private String login;
	
	private String password;
	
	public AccountRequest(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	public void login(String login, String password) {
		if(this.login.equals(login) && this.password.equals(password)) {
			reply(success);
		} else
			complaint(LoginFail.WrongLoginOrPassword);
	}
	
	public static class LoginSuccess {
		private LoginSuccess() { }
	}
	
	private final static LoginSuccess success = new LoginSuccess();
	
	public enum LoginFail {
		WrongLoginOrPassword,
		AccountInUse
	}
}
