package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;

public class AccountSample implements Context {

	public void define() {
		Reference account = create("account", new Account("root", "root"));
		account.send("logout");
		account.send("login", "root", "1243");
		account.send("login", "root", "root");
		account.send("login", "root", "root");
		account.send("logout");
		account.send("logout");
		account.send("login", "root", "root");
	}
	
	public static void main(String...args) {
		Model.launch(new AccountSample());
	}
	
}
