package fact.samples;

import fact.Context;
import fact.Model;

public class AcquaintancesSample implements Context {

	public void define() {
		acq("test", create("test", new Test()));
		acq("test").send("print");
		acq("test0").send("print");
		actor("test").send("print");
		actor("test0").send("print");
		actor("/root/test").send("print");
		actor("/root/test0").send("print");
	}
	
	public static class Test {
		
		public void print() {
			System.out.println("hello world");
		}
		
	}
	
	public static void main(String...args) {
		Model.launch(new AcquaintancesSample());
	}
}
