package fact.samples;

import fact.Context;
import fact.Model;

public class SharedDispatcher implements Context {
	static int count = 0;
	
	public void define() {
		dispatcher("shared", Test.class);
		for(int i = 0 ; i < 1000 ; i++)
			create("test-"+i, new Test());
	}
	
	public static class Test implements Context {
		
		public void define() {
			send("tick");
		}
		
		public void tick() {
			count++;
			System.out.println(count);
			count--;
			send("tick");
		}
	}
	
	public static void main(String...args) {
		Model.launch(new SharedDispatcher());
	}
}
