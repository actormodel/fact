package fact.samples.network.echo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.UUID;

import fact.Context;
import fact.Reference;
import fact.net.TcpListener;

public class EchoServer implements TcpListener, Context {

	@Override
	public void start() {
		acq("socket", sender());
	}
	
	@Override
	public void accept(Reference socket, InetSocketAddress address) {
		socket.send("register", create("echo-client-"+UUID.randomUUID(), new EchoClient()));
	}

	@Override
	public void error(IOException e) {
		System.out.println("error " + e);
	}

	@Override
	public void closed() {
		System.out.println("server closed");
	}

}
