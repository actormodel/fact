package fact.samples.network.echo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fact.Context;
import fact.net.TcpClient;

public class EchoConnection implements TcpClient, Context{
	public static String word = "hello world";
	
	@Override
	public void read(ByteBuffer buffer) {
		System.out.println("echo-connection: " + Main.readString(buffer));
	}

	@Override
	public void closed() {
		
	}

	@Override
	public void connected() {
		acq("socket", sender());
		become("Writing");
		send("write").delay(100);
	}

	@Override
	public void error(IOException e) {
		System.out.println("connection error " + e);
	}
	
	public class Writing {
		
		public void write() {
			final ByteBuffer buffer = ByteBuffer.allocate(1024).order(ByteOrder.LITTLE_ENDIAN);
			buffer.clear();
			Main.writeString(buffer, word);
			buffer.flip();
			acq("socket").send("write", buffer);
			send("write").delay(100);
		}
	}

}
