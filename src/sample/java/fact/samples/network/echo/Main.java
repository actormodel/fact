package fact.samples.network.echo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import fact.Context;
import fact.Model;
import fact.net.Network;

public class Main implements Context {

	public void define() {
		actor("/network").send("listen", "127.0.0.1", 2106, create("echo-server", new EchoServer()));
		send("connection").delay(1000);
	}
	
	public void connection() {
		actor("/network").send("connect", "127.0.0.1", 2106, create("echo-connection", new EchoConnection()));
	}
	
	public static void main(String...args) {
		Model.launch("network", new Network());
		Model.launch(new Main());
	}
	
	public static void writeString(ByteBuffer buffer, String str) {
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putInt(str.getBytes().length).put(str.getBytes());
	}
	
	public static String readString(ByteBuffer buffer) {
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		final byte[] bytes = new byte[buffer.getInt()];
		buffer.get(bytes);
		return new String(bytes);
	}
}
