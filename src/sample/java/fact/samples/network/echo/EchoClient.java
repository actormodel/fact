package fact.samples.network.echo;

import java.io.IOException;
import java.nio.ByteBuffer;

import fact.Context;
import fact.net.TcpClient;

public class EchoClient implements TcpClient, Context {

	@Override
	public void error(IOException e) {
		System.out.println("echo-client error: " + e);
	}

	@Override
	public void closed() {
		
	}

	@Override
	public void read(ByteBuffer buffer) {
		final String str = Main.readString(buffer);
		System.out.println("echo-client " + str);
		buffer.clear();
		Main.writeString(buffer, str);
		buffer.flip();
		acq("socket").send("write", buffer);
	}

	@Override
	public void connected() {
		acq("socket", sender());
	}


}
