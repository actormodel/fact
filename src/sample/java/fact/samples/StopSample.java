package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;

public class StopSample implements Context {
	private Reference test;
	
	public void define() {
		test = create("test", new TestActor());
		send("f1").delay(1000);
		test.send("print");
	}
	
	public void f1() {
		test.stop();
		test.send("print");
		send("f2").delay(1000);
	}
	
	public void f2() {
		test.resume();
		test.send("print2");
	}
	
	public static class TestActor {
		
		public void stop() {
			System.out.println("stop");
		}
		
		public void resume() {
			System.out.println("resume");
		}
		
		public void print() {
			System.out.println("print");
		}
		
		public void print2() {
			System.out.println("print2");
		}
	}
	
	public static void main(String...args) {
		Model.launch(new StopSample());
	}
	
}
