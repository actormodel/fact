package fact.samples;

import static fact.ActorContext.*;

import fact.Model;
import fact.Reference;

public class RequestSample0 {
	
	public void define() {
		create("a", new A())
			.send("target", 
				create("b", new B())
			);
	}
	
	public static class A {
	
		public void target(Reference target) {
			target.request("rq").handle("RequestHandle");
		}
		
		public class RequestHandle {
			
			public void reply(String message) {
				System.out.println("reply: " + message);
			}
		}
		
	}
	
	public static class B {
		
		public void rq() {
			reply("hello world!");
		}
		
	}
	
	public static void main(String...args) {
		Model.launch(new RequestSample0());
	}
	
}
