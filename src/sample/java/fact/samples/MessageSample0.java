package fact.samples;

import fact.Context;
import fact.Model;

public class MessageSample0 implements Context {

	public void define() {
		send("print", Byte.MIN_VALUE);
		send("print", Byte.MAX_VALUE);
		send("print", Short.MIN_VALUE);
		send("print", Short.MAX_VALUE);
		send("print", Integer.MIN_VALUE);
		send("print", Integer.MAX_VALUE);
		send("print", Long.MIN_VALUE);
		send("print", Long.MAX_VALUE);
		send("print", Float.MIN_VALUE);
		send("print", Float.MAX_VALUE);
		send("print", Double.MIN_VALUE);
		send("print", Double.MAX_VALUE);
		send("print", Boolean.FALSE);
		send("print", Boolean.TRUE);
		for(char c : "hello world".toCharArray())
			send("print", c);
		send("print", "hello world!");
	}
	
	public void print(byte value) { System.out.println("byte " + value); }
	public void print(short value) { System.out.println("short " + value); }
	public void print(int value) { System.out.println("int " + value); }
	public void print(long value) { System.out.println("long " + value); }
	public void print(float value) { System.out.println("float " + value); }
	public void print(double value) { System.out.println("double " + value); }
	public void print(String value) { System.out.println("string " + value); }
	public void print(boolean value) { System.out.println("boolean " + value); }
	public void print(char value) { System.out.println("char " + value); }
	
	public static void main(String...args) {
		Model.launch(new MessageSample0());
	}
}
