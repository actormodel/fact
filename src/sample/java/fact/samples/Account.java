package fact.samples;

import fact.Context;

public class Account implements Context {
	final String login;
	
	final String password;
	
	Account(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	public void define() {
		become("Free");
	}
	
	public void logout() {
		System.out.println("wrong logout");
	}
	
	public class Free {
		
		public void login(String login, String password) {
			if(Account.this.login.equals(login) && Account.this.password.equals(password)) {
				System.out.println("login success");
				become("Login");
			} else
				System.out.println("wrong login or password");
		}
		
	}
	
	public class Login {
		
		public void login(String login, String password) {
			System.out.println("account in use");
		}
		
		public void logout() {
			System.out.println("logout");
			become("Free");
		}
	}
	public static class LoginSuccess {
		private LoginSuccess() { }
	}
	
	private final static LoginSuccess success = new LoginSuccess();
	
	public enum LoginFail {
		WrongLoginOrPassword,
		AccountInUse
	}
}
