package fact.samples;

import fact.Context;
import fact.Model;

public class UnhandledMessage implements Context {

	public void define() {
		send("test");
	}
	
	public static void main(String...args) {
		Model.launch(new UnhandledMessage());
	}
}
