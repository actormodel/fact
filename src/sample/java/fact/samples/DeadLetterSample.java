package fact.samples;

import fact.Context;
import fact.Model;

public class DeadLetterSample implements Context {

	public void define() {
		kill();
		send("test");
	}
	
	public static void main(String...args) {
		Model.launch(new DeadLetterSample());
	}
}
