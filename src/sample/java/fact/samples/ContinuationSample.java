package fact.samples;

import static fact.ActorContext.*;

import fact.ActorContext;
import fact.Model;

public class ContinuationSample {

	public static class A {
		
		public void define() {
			System.out.println("define A");
			acq("b", create("b", new B()));
			send("rq0");
			send("rq1");
			send("rq");
		}
		
		//single request
		public void rq0() {
			acq("b").request("rq0").handle("Request");
		}
		
		//request chain
		public void rq1() {
			acq("b").request("rq1").handle("Request");
		}
		
		//continuation
		public void rq() {
			acq("b").request("rq").handle("Request");
		}
		
		public class Request {
			
			public void reply() {
				System.out.println("actor A receive reply from " + sender());
			}
			
		}
	}
	
	public static class B {
		
		public void define() {
			System.out.println("define B");
			acq("c", create("c", new C()));
		}
		
		//simple request(one chain)
		public void rq0() {
			System.out.println("request B.rq0");
			reply();
		}
		
		//request  chain
		public void rq1() {
			System.out.println("request B.rq1");
			acq("c").request("rq").handle("Request");;
		}
		
		//continuation
		public void rq() {
			System.out.println("request B.rq");
			continuation(acq("c"));
		}
		
		public class Request {
			public void reply() {
				System.out.println("actor B receive reply from " + sender());
				ActorContext.reply();
			}
		}
	}
	
	public static class C {
		
		public void define() {
			System.out.println("define C");
		}
		
		public void rq() {
			reply();
		}
		
	}
	
	public static void main(String...args) {
		Model.launch(new A());
	}
}
