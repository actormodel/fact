package fact.samples;

import java.util.LinkedList;

import fact.Context;
import fact.Model;
import fact.Reference;

public class DumpTreeSample implements Context{
	public final int N = 5;
	
	private LinkedList<Reference> list = new LinkedList<>();
	
	public void define() {
		for(int i = 0 ; i < N ; i++)
			list.add(create("node-"+N+"-"+i, new A(N, i)));
		send("dump0").delay(1000);
		send("test0").delay(2000);
	}
	
	public void dump0() {
		dump();
	}
	
	public void test0() {
		list.forEach(item -> item.kill());
		send("dump0").delay(1000);
	}
	
	public static class A implements Context {
		public final int n;
		
		public final int i;
		
		public A(int n, int i) {
			this.n = n;
			this.i = i;
		}
		
		public void define() {
			System.out.println("#define node-"+n+"-"+i);
			if(n != 0) {
				for(int x = 0 ; x < n ; x++)
					create("node-"+(n-1)+"-"+ x, new A(n-1, x));
			}
		}
		
		public void terminate() {
			System.out.println("#terminate node-"+n+"-"+i);
		}
	}
	
	public static void main(String...args) {
		Model.launch(new DumpTreeSample());
	}
}
