package fact.samples;

import fact.Context;
import fact.Model;
import fact.Reference;
import fact.samples.AccountRequest.LoginFail;
import fact.samples.AccountRequest.LoginSuccess;

public class RequestSample implements Context {
	Reference account;
	
	public void define() {
		account = create("account", new AccountRequest("root", "root"));
		account.request("login", "login", "login").handle("Request");
		send("tick");
		send("print");
	}
	
	public void tick() {
		account.request("login", "root", "root").handle("Request");
		send("print");
	}
	
	public void print() {
		System.out.println("print");
	}
	
	public class Request {
		
		public void reply(LoginFail fail) {
			System.out.println("login failed");
		}
		
		public void reply(LoginSuccess success) {
			System.out.println("login success");
		}
	}
	
	public static void main(String...args) {
		Model.launch(new RequestSample());
	}
}
